import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.runner.RunWith

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import internal.GlobalVariable as GlobalVariable


CucumberKW.runWithCucumberRunner(MyCucumberRunner.class)

@RunWith(Cucumber.class)
@CucumberOptions(features="Include/features", glue="", plugin = ["pretty",
					 "junit:C:/New Report/cucumber.xml",
					 "html:C:/New Report",
					 "json:C:/New Report/cucumber.json"])
public class MyCucumberRunner {
	public void setPrintln(java.lang.Object println) {
		this.println = println;
	}
}