package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object ShahidURL
     
    /**
     * <p></p>
     */
    public static Object Header
     
    /**
     * <p></p>
     */
    public static Object SubText1
     
    /**
     * <p></p>
     */
    public static Object SignUpText
     

    static {
        def allVariables = [:]        
        allVariables.put('default', ['ShahidURL' : 'https://shahid.mbc.net/ar/widgets/#/login'])
        allVariables.put('LoginProfile', allVariables['default'] + ['Header' : 'تسجيل الدخول', 'SubText1' : 'سجل الدخول عبر بريدك الالكتروني او عبر حسابك على الفيسبوك', 'SignUpText' : 'إذا لم يكن لديك حساب في SHAHID, قم بإنشاء حساب جديد', 'ShahidURL' : 'https://shahid.mbc.net/ar/widgets/#/login'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        ShahidURL = selectedVariables['ShahidURL']
        Header = selectedVariables['Header']
        SubText1 = selectedVariables['SubText1']
        SignUpText = selectedVariables['SignUpText']
        
    }
}
