$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("C:/KatalonWS/Shahid/Include/features/ShahidLogin.feature");
formatter.feature({
  "name": "Login Feature POC",
  "description": "  I want to use this template for my feature file",
  "keyword": "Feature",
  "tags": [
    {
      "name": "@shahid"
    }
  ]
});
formatter.scenarioOutline({
  "name": "Login with Valid Credintials",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "I want to navigate to shahid login page",
  "keyword": "Given "
});
formatter.step({
  "name": "I Enter valid username \u003cusername\u003e and valid password \u003cpassword\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "I Enter the \u003ccaptcha\u003e",
  "keyword": "And "
});
formatter.step({
  "name": "I click on login button",
  "keyword": "And "
});
formatter.step({
  "name": "Home Page to be verified",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "username",
        "password",
        "captcha"
      ]
    },
    {
      "cells": [
        "newaccount211@mbc.net",
        "ram1101994",
        "Captcha"
      ]
    },
    {
      "cells": [
        "RamezQa@mbc.net",
        "ram1101994",
        "Captcha"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Login with Valid Credintials",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@shahid"
    },
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "I want to navigate to shahid login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.NavigationStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I Enter valid username newaccount211@mbc.net and valid password ram1101994",
  "keyword": "When "
});
formatter.match({
  "location": "LoginSteps.loginDataEntry(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I Enter the Captcha",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.captchaDataEntry(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on login button",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.clickonLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Home Page to be verified",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.verifyHomePageElements()"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Login with Valid Credintials",
  "description": "",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "name": "@shahid"
    },
    {
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "name": "I want to navigate to shahid login page",
  "keyword": "Given "
});
formatter.match({
  "location": "LoginSteps.NavigationStep()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I Enter valid username RamezQa@mbc.net and valid password ram1101994",
  "keyword": "When "
});
formatter.match({
  "location": "LoginSteps.loginDataEntry(String,String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I Enter the Captcha",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.captchaDataEntry(String)"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "I click on login button",
  "keyword": "And "
});
formatter.match({
  "location": "LoginSteps.clickonLoginButton()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Home Page to be verified",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginSteps.verifyHomePageElements()"
});
formatter.result({
  "status": "passed"
});
});