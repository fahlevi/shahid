#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@shahid
Feature: Login Feature POC
  I want to use this template for my feature file

  @tag1
  Scenario Outline: Login with Valid Credintials
    Given I want to navigate to shahid login page
    When I Enter valid username <username> and valid password <password>
    And I Enter the <captcha> 
    And I click on login button
    Then Home Page to be verified

    Examples: 
      | username  								|password 	| captcha 		|
      | newaccount211@mbc.net 		|ram1101994 | Captcha		  |
      | RamezQa@mbc.net						|ram1101994 | Captcha     |